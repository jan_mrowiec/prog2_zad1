#include <iostream>
#include <string>

using namespace std;


void surname_char(char** t, int n)
{
    char in[100];
    while(n--)
    {
        cin >> in;
        int s = 1;
        while(in[s] != 0)
            s++;
        *t = new char[s];
        for(int i = 0; i < s; i++)
            (*t)[i] = in[i];
        t++;
    }
}

int main()
{
    int n = 5;
    char** t = new char* [n];
    surname_char(t, n);
    cout << endl;
    for(int i = 0; i < n; i++)
        cout << t[i] << endl;
    for(int i = 0; i < n; i++)
        delete [] t[i];
    delete [] t;
    return 0;
}