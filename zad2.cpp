#include <iostream>
#include <string>
#include <memory>
using namespace std;

struct struct_two{
    string s = "Text";
};

struct struct_one{
    int a = 5;
    shared_ptr < struct_two > p;
};

int main()
{
    struct_one one;
    one.p = make_shared<struct_two> ();
    cout << one.p.get()->s << endl;
    one.p.get()->s = "inny text";
    cout << one.p.get()->s << endl;
    shared_ptr <struct_two> p;
    p = one.p;
    cout << p.get()->s << endl;

    return 0;
}
