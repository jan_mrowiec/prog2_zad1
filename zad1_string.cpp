#include <iostream>
#include <string>

using namespace std;


void surname_string(string* t, int n)
{
    while(n--)
    {
        cin >> *t;
        t++;
    }
}

int main()
{
    int n = 5;
    string ts[5];
    surname_string(ts, n);
    cout << endl;
    for(int i = 0; i < n; i++)
    {
        cout << ts[i] << endl;
    }
    return 0;
}